#!bin/bash

# To set frontpage limit
frontpage=$1

# Build
docker build -t "webserver:latest" --build-arg fpLimit=$frontpage .

# Delete Existing
docker stop dwww1 dwww2 dwww3
docker rm dwww1 dwww2 dwww3


# Create Dockers
docker run -P --name "dwww1" --restart unless-stopped -p 32768:80 -d webserver:latest
docker run -P --name "dwww2" --restart unless-stopped -p 32769:80 -d webserver:latest
docker run -P --name "dwww3" --restart unless-stopped -p 32770:80 -d webserver:latest
#docker run -P --name "dwwwFallback" --restart unless-stopped -p 32771:80 -d fallbackwebserver:v$fversion

echo "Docker deployment complete."