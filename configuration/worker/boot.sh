#!/bin/bash
source /home/ubuntu/dcsg2003/configuration/base.sh

ownIp=$(hostname -I | awk '{ print $1 }' | xargs)

# Mount volume if not already
mount -t glusterfs $ownIp:bf_config /bf_config
mount -t glusterfs $ownIp:bf_images /bf_images

chmod 777 /bf_config
chmod 777 /bf_images

sleep 5

# Check if mount is successful
diskImages="bf_images"
diskConfig="bf_config"

if [ $(df -h | grep "$diskImages" | wc -l) -ne 0 ]
then
    echo "$diskImages is mounted"
else
    echo "$diskImages is not mounted"
    discord_log "Failed to mount disk."
    exit
fi

if [ $(df -h | grep "$diskConfig" | wc -l) -ne 0 ]
then
    echo "$diskConfig is mounted"
else
    echo "$diskConfig is not mounted"
    discord_log "Failed to mount disk."
    exit
fi

# Start cockroach db
yes | bash /home/ubuntu/startdb.sh

# Start Docker
systemctl start docker