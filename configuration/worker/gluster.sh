#!/bin/bash
# This script is run on one worker, and adds the worker with ip provided in second parameter to the gluster cluster as well as both volumes.
# It's important that the ip of the new node is added to glusterListIp BEFORE "gluster join" is run. 
# Similarly, it's imporant that the ip is removed from glusterListIp AFTER the "gluster leave" command is run.

source /home/ubuntu/dcsg2003/configuration/base.sh

ipList=$(cat /home/ubuntu/glusterListIp)
ownIp=$(hostname -I | awk '{ print $1 }' | xargs)
paramIp=$2 #Ip of machine you wish to add or remove from cluster

startVolumes () {
    gluster volume start bf_images
    gluster volume start bf_config
}

#Stop both gluster columes
#echo y | gluster volume stop bf_images
#echo y | gluster volume stop bf_config

# Script for removing/adding machines to gluster cluster.
if [[ $1 == "join" ]]
then
    workerAmount=$(echo $(($(cat /home/ubuntu/glusterListIp | wc -w) + 1)))

    #Join cluster
    gluster peer probe $paramIp
    if [ $? -ne 0 ]
    then
        discord_log "Error in adding node to gluster cluster."
        startVolumes
        exit
    fi

    echo "Sleeping for 3 seconds"
    sleep 3

    #Add volume
    yes | printf 'y\n%.0s' {1..3} | gluster volume add-brick bf_images replica $workerAmount $paramIp:/bf_brick force
    yes | printf 'y\n%.0s' {1..3} | gluster volume add-brick bf_config replica $workerAmount $paramIp:/config_brick force

elif [[ $1 == "leave" ]]
then
    workerAmount=$(cat /home/ubuntu/glusterListIp | wc -w)

    #Remove volumes
    yes | printf 'y\n%.0s' {1..3} | gluster volume remove-brick bf_images replica $workerAmount $paramIp:/bf_brick force
    yes | printf 'y\n%.0s' {1..3} | gluster volume remove-brick bf_config replica $workerAmount $paramIp:/config_brick force
    if [ $? -ne 0 ]
    then
        discord_log "Error in removing volume from gluster node."
        startVolumes
        exit
    fi

    #Leave cluster
    yes | printf 'y\n%.0s' {1..3} | gluster peer detach $paramIp
    if [ $? -ne 0 ]
    then
        discord_log "Error in removing node from gluster cluster."
        startVolumes
        exit
    fi

else
    echo "Invalid argument $1"
fi

#Start both gluster columes
startVolumes