#!/bin/bash
# Run this command on one worker to initialize both gluster volumes if completely empty.
source /home/ubuntu/dcsg2003/configuration/base.sh

ipList=$(cat /home/ubuntu/glusterListIp)
ownIp=$(hostname -I | awk '{ print $1 }' | xargs)
workerAmount=$(echo $(($(cat /home/ubuntu/glusterListIp | wc -w) + 1)))

bf_imagesCommand="gluster volume create bf_images replica $workerAmount $ownIp:/bf_brick"
bf_configCommand="gluster volume create bf_config replica $workerAmount $ownIp:/config_brick"

for ip in $ipList
do
    bf_imagesCommand="$bf_imagesCommand $ip:/bf_brick"
    bf_configCommand="$bf_configCommand $ip:/config_brick"
done
bf_imagesCommand="$bf_imagesCommand force"
bf_configCommand="$bf_configCommand force"

echo $bf_imagesCommand
echo $bf_configCommand

$bf_imagesCommand
$bf_configCommand

gluster volume start bf_images
gluster volume start bf_config