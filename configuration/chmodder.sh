#!/bin/bash
files=$(find ../ -name "*.sh")

for file in $files
do
    chmod +x $file
    echo "Updated permission for $file"
done