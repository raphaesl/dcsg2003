#!/bin/bash
crVersion="22.2.5" #Cockroach database version

# Older versions of Docker went by the names of docker, docker.io, or docker-engine. Uninstall any such older versions before attempting to install a new version:
apt-get -y remove docker docker-engine docker.io containerd runc

# Update the apt package index and install packages to allow apt to use a repository over HTTPS:
sudo apt-get -y update
sudo apt-get -y install \
    ca-certificates \
    curl \
    git \
    gnupg \
    lsb-release
apt-get -y update

# Use the following command to set up the repository:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get -y update

# Fix potentially broken mask
chmod a+r /etc/apt/keyrings/docker.gpg

# Install docker engine
apt-get install -y apt-transport-https ca-certificates software-properties-common python3-openstackclient python3-octaviaclient docker-ce
apt-get -y update

# Install Docker Engine, containerd, and Docker Compose.
#sudo apt-get -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin

# Fix potentially broken docker build
echo "{ \"insecure-registries\" : [\"192.168.128.23:5000\"] }" > /etc/docker/daemon.json

# Disable Automatic Docker start
systemctl disable docker
systemctl disable docker.service
systemctl disable docker.socket

# Add to docker swarm cluster as worker
#docker swarm join --token SWMTKN-1-5msr1ct8s2de7vrwkj9c2zzcx50gvvg1e1ty8vvc10eiyarfic-6x253ue84qc8psdwqjnacuwll 192.168.134.127:2377

# Sync time using NTP
apt-get install -y ntpdate
ntpdate -b ntp.justervesenet.no

# Set correct timezone
timedatectl set-timezone Europe/Oslo

# Authenticate manager for root user
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDPU7c1M98QrF5D52HBMBHDaiDvTFEvA6Sdonl4oqCn3NavjmCM5PZBk4No7z8H4SX3P6VU98RE7pb/Xug5EMa9AQImWTRzausAShtx0L9yGoXzOXZjctFcTDoN60jV2/Bc8ai8e48A7x1Zt89Hcvc+ktE5XdlGUXVXrTK26JPHwn5EkEti4rXlhTf42jJwYGS97vgiLxQGlTRs5/5e4d9zj2iwAb5m/ml3TxsBpnauTcCeIYTN2R8b6Cl6DIwRFiYFW+lAoR395vrDayD+VPjUS9S1Gqfn73deV39vx0Kzqor0KuA2kqL5N2YMXwMP3lTOek98hJuFURO1bfQIkc4kJNJfumka82NlwwNIsJIITci/+pNgqiWHhTyPcFMsFNFKJoQf5idNYT6PAGZmcsMOqqonTXfd65Pn91qeiq25I3cpgXZ/Z4S5VmjR20a0roTe40o3sho7Z6fzAIhKTNhcE/zDjBntKwRH8n73RpyFNO8zYXCa4lRn8YSmtwfTMFk= ubuntu@manager" > /root/.ssh/authorized_keys

# Download and install cockroach database
wget https://binaries.cockroachdb.com/cockroach-v$crVersion.linux-amd64.tgz 
tar xzf cockroach-v$crVersion.linux-amd64.tgz 
cp cockroach-v$crVersion.linux-amd64/cockroach /usr/local/bin 
mkdir /bfdata

# Download and install glusterFS
apt-get -y install glusterfs-server glusterfs-client
systemctl enable glusterd
systemctl start glusterd

# Make brick directories for glusterfs
mkdir /bf_brick
mkdir /config_brick

# Make brick mounting directories
mkdir /bf_images
mkdir /bf_config

# Clone git repository
git clone https://glpat-yEr3xU4_23z-jJ6UGC29@gitlab.stud.idi.ntnu.no/raphaesl/dcsg2003.git /home/ubuntu/dcsg2003
git config --global --add safe.directory /home/ubuntu/dcsg2003
chown -R ubuntu:ubuntu /home/ubuntu/dcsg2003

# Clone bookface repository
git clone https://github.com/hioa-cs/bookface.git /home/ubuntu/bookface
chown -R ubuntu:ubuntu /home/ubuntu/bookface

# Make Scripts Executable
chmod +x /users/ubuntu/dcsg2003/configuration/chmodder.sh 
bash /users/ubuntu/dcsg2003/configuration/chmodder.sh

# Set crontab file
crontab < /home/ubuntu/dcsg2003/configuration/worker/cron

# Copy haproxy.cfg to bookface
cp /home/ubuntu/dcsg2003/configuration/worker/haproxy.cfg /home/ubuntu/bookface/

# Another update for good measure
apt-get -y update