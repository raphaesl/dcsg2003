RED='\033[0;31m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
YELLOW='\033[1;33m'
NC='\033[0m' # No Color

info () {
echo -e "${CYAN}$1${NC}"
}

error () {
echo -e "${RED}$1${NC}"
}

warn () {
echo -e "${YELLOW}$1${NC}"
}

ok () {
echo -e "${GREEN}$1${NC}"
}

# Paste URL below
infoURL="https://discord.com/api/webhooks/1080132317059760149/C1-xRUqiWiuRPwKuwzrDnJWYELi3vLO0fB01Npl1puZMsxzaFYte61Z69lxcG6CcN_9h"
errorURL="https://discordapp.com/api/webhooks/1085532834325872700/InMahhZzTVDg-hsX-bhTaWYfTqhxli7ZVU6dOmLtkOICSFQmNDP-6wvVlt9ISlpb-u7C"

discord_log () {

message="$1"

USERNAME="Logger [$HOSTNAME] ${0##*/}"

JSON="{\"username\": \"$USERNAME\", \"content\": \"$message\"}"

curl -s -X POST -H "Content-Type: application/json" -d "$JSON" $infoURL

}

discord_error () {

message="$1"

USERNAME="Logger [$HOSTNAME] ${0##*/}"

JSON="{\"username\": \"$USERNAME\", \"content\": \"$message\"}"

curl -s -X POST -H "Content-Type: application/json" -d "$JSON" $errorURL

}