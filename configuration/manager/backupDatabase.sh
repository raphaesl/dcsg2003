#!bin/bash
source /home/ubuntu/DCSG2003_V23_group45-openrc.sh
source /home/ubuntu/dcsg2003/configuration/base.sh

name="backup"
ip=$(openstack server list --name $name | tail -2 | head -1 | cut -d'|' -f5  | grep -o -P '(?<=192.168.).*(?= )' | awk '{print "192.168."$0}' | xargs)

discord_log "Initiating daily backup..."

# Start backup server
openstack server start $name

# Wait for valid ssh connection
until ssh root@$ip exit
do
    sleep 5
done

# Execute backup script
ssh root@$ip bash /home/ubuntu/dcsg2003/configuration/backupServer/backup.sh

# Turn off backup server
openstack server stop $name