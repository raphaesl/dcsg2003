#!/bin/bash
source /home/ubuntu/DCSG2003_V23_group45-openrc.sh
source /home/ubuntu/dcsg2003/configuration/base.sh

floatingIp="10.212.169.121"
fallBackName="www3"
fallBackPort="32771"

if [[ $1 == "up" ]]
then
    openstack server remove floating ip www3 $floatingIp
    openstack server add floating ip balancer $floatingIp 

    echo "Bookface has succcessfully reinstated production"
elif [[ $1 == "down" ]]
then
    openstack server remove floating ip balancer $floatingIp
    openstack server add floating ip $fallBackName $floatingIp

    echo "Bookface has succcessfully closed production"
else
echo "Error: Invalid argument \"$1\""
fi
