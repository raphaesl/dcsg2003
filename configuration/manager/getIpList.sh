#!/bin/
source /home/ubuntu/DCSG2003_V23_group45-openrc.sh

instances=$(($(openstack server list --ip 168 --status "ACTIVE" --name "[^manager]" -n | wc -l) - 4))
openstack server list --ip 168 --status "ACTIVE" --name "[^manager ^backup]" -n | tail -$(($instances + 1)) | head -$instances | cut -d'|' -f5 | grep -o -P '(?<=192.168.).*(?= )' | awk '{print "192.168."$0}' > /home/ubuntu/logs/ipList