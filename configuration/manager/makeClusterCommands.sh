#!/bin/bash
source /home/ubuntu/DCSG2003_V23_group45-openrc.sh
source /home/ubuntu/dcsg2003/configuration/base.sh

# 1) Hent inn alle aktuelle ip addresser
#ipList=$(cat /home/ubuntu/logs/ipList)
ipList="192.168.134.127 192.168.134.194 192.168.130.56"

# 2) Lag startup kommando for hver ip addresse
for mainIp in $ipList
do
    glusterListIp=""
    kommando="cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join="

    for ip in $ipList
    do
        if [[ $ip != $mainIp ]]
        then 
            glusterListIp="$glusterListIp $ip"
        fi

        kommando="$kommando$ip:26257,"
    done
    kommando=$(echo "$kommando" | sed 's/,$//')
    kommando="$kommando --advertise-addr=$mainIp:26257 --max-offset=1500ms"

    # 3) Via ssh, send startup kommando til fil i home
    ssh -o "StrictHostKeyChecking no" root@$mainIp "echo $kommando > /home/ubuntu/startdb.sh"
    # Via ssh, send lista over ip-adresser i glusteren til aktuelle server
    ssh -o "StrictHostKeyChecking no" root@$mainIp "echo $glusterListIp > /home/ubuntu/glusterListIp"
done