#!/bin/bash
git -C /home/ubuntu/dcsg2003/ fetch --all
git -C /home/ubuntu/dcsg2003/ reset --hard origin/main
bash /home/ubuntu/dcsg2003/configuration/chmodder.sh

ipList=$(cat /home/ubuntu/logs/ipList)

echo "Found $(echo $ipList | wc -w) active VM's."
echo "$(date) [pollGit] Fetching git repository for $(echo $ipList | wc -w) active VM's: $(echo $ipList | tr -d '\n')" >> /home/ubuntu/dcsg2003/configuration/manager/pollGit.log

for ip in $ipList
do
    echo "Polling $ip..."
    ssh -o "StrictHostKeyChecking no" ubuntu@$ip sudo git -C /home/ubuntu/dcsg2003/ fetch --all
    ssh -o "StrictHostKeyChecking no" ubuntu@$ip sudo git -C /home/ubuntu/dcsg2003/ reset --hard origin/main
done

crontab < /home/ubuntu/dcsg2003/configuration/manager/cron
if [ $? != 0 ]
then
    discord_error "Couldn't update crontab to new version."
fi