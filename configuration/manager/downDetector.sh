#!/bin/bash

source /home/ubuntu/DCSG2003_V23_group45-openrc.sh
source /home/ubuntu/dcsg2003/configuration/base.sh

echo "$(date) [downDetector] Polling VM's..." >> /home/ubuntu/logs/health.log
productionDown="0"

ipList=$(cat /home/ubuntu/logs/ipList)
for ip in $ipList
do
    ssh -o "StrictHostKeyChecking no" -q ubuntu@$ip exit
    if [ $? -ne 0 ]
    then
        name=$(openstack server list --ip $ip | cut -d'|' -f3 | head -4 | tail -1 | xargs)
        if [ -z "$name" ]
        then
            name="N/A"
        fi

        discord_log "VM $name:$ip is currently offline. Restarting..."

        if [[ $productionDown == "0" ]]
        then
        productionDown="1"
        bash /home/ubuntu/dcsg2003/configuration/manager/production.sh down
        fi
        
        openstack server start $name

        if [ $? -ne 0 ]
        then
            discord_error "Failed to restart VM $name:$ip"
        fi
    fi
done

if [[ $productionDown == "1" ]]
then
bash /home/ubuntu/dcsg2003/configuration/manager/production.sh up
fi
