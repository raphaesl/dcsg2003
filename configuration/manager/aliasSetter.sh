#!/bin/bash
source /home/ubuntu/DCSG2003_V23_group45-openrc.sh
source /home/ubuntu/dcsg2003/configuration/base.sh

openstackSearch=$(openstack server list --name [^manager])
ipList=$(cat /home/ubuntu/logs/ipList)

for ip in $ipList
do
    name="$(echo $openstackSearch | grep $ip | cut -d'|' -f3 | xargs)"
    alias $name="ssh ubuntu@$ip"
done

