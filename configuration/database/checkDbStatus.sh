#!/bin/bash
lines=$(ps aux | grep  cockroach | wc -l) #If there is two lines in the result, it means the database is operational.
if [ $lines -ne 2 ]
then
        echo $(date)": The database is offline. Restarting..." >> /home/ubuntu/checkDbStatus.log
        sudo cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join=localhost:26257 > /dev/null 2>&1 &
else
        echo $(date)": Database is online. No action is being taken." >> /home/ubuntu/checkDbStatus.log
fi