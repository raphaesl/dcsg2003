#!/bin/bash

userCount=$(cockroach --insecure --host=localhost sql --execute="use bf;SELECT COUNT(userid) FROM users;" | head -3 | tail -1)
postCount=$(cockroach --insecure --host=localhost sql --execute="use bf;SELECT COUNT(postid) FROM posts;" | head -3 | tail -1)
commentCount=$(cockroach --insecure --host=localhost sql --execute="use bf;SELECT COUNT(commentid) FROM comments;" | head -3 | tail -1)

echo "Users: $userCount Posts: $postCount Comments: $commentCount"