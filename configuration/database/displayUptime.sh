#!/bin/bash

online=$(tail -n 288 checkDbStatus.log | grep "online" | wc -l)
offline=$(tail -n 288 checkDbStatus.log | grep "offline" | wc -l)
total=$(($offline+$online))

echo "Database has "$((($online*100)/$total))"% uptime for the last 24 hours. There is "$offline" records of the database being offline."