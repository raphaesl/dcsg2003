#!/bin/bash
source /home/ubuntu/dcsg2003/configuration/base.sh

dbIp="192.168.130.56"
ssh -o "StrictHostKeyChecking no" ubuntu@$dbIp exit

# Manager turns on backup-server.

# Pull git
git -C /home/ubuntu/dcsg2003/ fetch --all
git -C /home/ubuntu/dcsg2003/ reset --hard origin/main

# Mount volume to backup
mount /dev/vdb /backup

# Deletes all but the 3 newests backups
ls -t bfdata* | tail -n +4 | xargs rm -f

# Turn off cockroach database
pid=$(ssh root@$dbIp ps aux | grep "cockroach" | grep -v "grep" | awk '{print $2}' | head -1)
ssh root@$dbIp kill $pid

# Delete temporary folder
rm -r /tmp/bfdata

# Copy bfdata to /tmp/bfdata
scp -r root@$dbIp:/bfdata /tmp/bfdata/

# Turn on cockroach database
ssh root@$dbIp bash /home/ubuntu/dcsg2003/configuration/worker/boot.sh

# Archive bfdata to zip
tar cvzf bfdata_backup_$(date +'%d-%m-%y_%H-%M').tgz -C /tmp/ bfdata/

# Copy zip file to volume /backup
cp ./bfdata_backup_* /backup