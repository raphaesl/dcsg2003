#!/bin/bash
dbIp="192.168.130.246"
ssh -o "StrictHostKeyChecking no" ubuntu@$dbIp exit

# Unzip data
rm -r /tmp/bfdata
tar xvzf /backup/bfdata_backup_$1.tgz -C /tmp

# Turn off cockroach database
pid=$(ssh root@$dbIp ps aux | grep "cockroach" | grep -v "grep" | awk '{print $2}' | head -1)
ssh root@$dbIp kill $pid

# Copy Data to database
ssh root@$dbIp rm -r /bfdata #!!!!!!!! <- DELETES CURRENT DATABASE! BIG DANGER
scp -r /tmp/bfdata root@$dbIp:/bfdata

# Turn on cockroach database
ssh -q root@$dbIp bash /home/ubuntu/dcsg2003/configuration/database/checkDbStatus.sh