#!/bin/bash

new=$(cksum /home/ubuntu/dcsg2003/configuration/balancer/haproxy.cfg | awk '{print $1 $2}')
old=$(cksum /etc/haproxy/haproxy.cfg | awk '{print $1 $2}')

if [ "$new" -ne "$old" ]
then
    rm /etc/haproxy/haproxy.cfg
    cp /home/ubuntu/dcsg2003/configuration/balancer/haproxy.cfg /etc/haproxy/
    service haproxy restart
    echo "$(date) [deployConfig] Detected change in configuration file. Deploying to haproxy now..." >> /home/ubuntu/deployConfig.log
else
    echo "$(date) [deployConfig] No change detected." >> /home/ubuntu/deployConfig.log
fi