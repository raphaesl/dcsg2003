# Prosedyre for starting/stopping av produksjon

1) På *manager* VM'en, bruk kommando *production down*. Dette vil redirekte trafikk til en alternativ host, og frigjøre bookface for store konfigurasjonsendringer.
2) Når du er ferdig, kjør kommandoen *production up*. Dette vil åpne trafikk tilbake til bookface.