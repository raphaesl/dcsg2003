# Week 5

## Task 1
### Installing Docker
To install Docker, read this link and do the "Install using the repository" method.
https://docs.docker.com/engine/install/ubuntu/

**NOTE** Before you install Docker Engine for the first time on a new host machine, you need to set up the Docker repository. Afterward, you can install and update Docker from the repository.

### First we set up the repository:

1. Update the apt package index and install packages to allow apt to use a repository over HTTPS:
```
sudo apt-get update

sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```

2. Add Docker’s official GPG key:
```
sudo mkdir -m 0755 -p /etc/apt/keyrings

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```

3. Use the following command to set up the repository:
```
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

### Install Docker Engine

1. Update the apt package index:
```sudo apt-get update```

If you are receveing a GPG-error when using the command above, you must give the Docker public key file read permission before updating the package index:
```
sudo chmod a+r /etc/apt/keyrings/docker.gpg
sudo apt-get update
```

2. Install Docker Engine, containerd, and Docker Compose:
```
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

3. Verify that the Docker Engine installation is successful by running the hello-world image:
```
sudo docker run hello-world
```
This command downloads a test image and runs it in a container. When the container runs, it prints a confirmation message and exits.

If you need to uninstall Docker, see the link at the top of this task.

## Task 2
By giving the tables indexes, we can optimize the database for faster lookups. We do this by running this command in cockroachDB sql-shell:

```create index on pictures (pictureid);```

By running the code explain SELECT picture FROM pictures WHERE pictureid = '7VAgCDqHlheLtNK5wjiApbjylVj63L.jpg';


<img src="./bilder/task2_w5.png">












### Docker Commands
```
# To create a volume
docker volume create apacheConfig

# To build an image
docker build -t "webserver:v0.12" .

# To run an image
docker run -P --name "dwww2" -d webserver:v0.8

# To run an image with shared config file
docker run -P --name "dwww1" -v ~/dcsg2003/docker/webserver/config.php:/var/www/html/config.php -d webserver:v0.9 

# Delete all containers
docker stop $(docker ps -qa)
docker rm $(docker ps -qa)


# Install memcache docker
docker run --name=memcache -p 11211:11211 -d 192.168.128.23:5000/memcached memcached -m <ram>

```


### Haproxy Config for using docker
```
global
        log /dev/log    local0
        log /dev/log    local1 notice
        chroot /var/lib/haproxy
        stats socket /run/haproxy/admin.sock mode 660 level admin expose-fd listeners
        stats timeout 30s
        user haproxy
        group haproxy
        daemon

        # Default SSL material locations
        ca-base /etc/ssl/certs
        crt-base /etc/ssl/private

        # See: https://ssl-config.mozilla.org/#server=haproxy&server-version=2.0.3&config=intermediate
        ssl-default-bind-ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
        ssl-default-bind-ciphersuites TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256
        ssl-default-bind-options ssl-min-ver TLSv1.2 no-tls-tickets

defaults
        log     global
        mode    http
        option  httplog
        option  dontlognull
        timeout connect 5000
        timeout client  50000
        timeout server  50000
        errorfile 400 /etc/haproxy/errors/400.http
        errorfile 403 /etc/haproxy/errors/403.http
        errorfile 408 /etc/haproxy/errors/408.http
        errorfile 500 /etc/haproxy/errors/500.http
        errorfile 502 /etc/haproxy/errors/502.http
        errorfile 503 /etc/haproxy/errors/503.http
        errorfile 504 /etc/haproxy/errors/504.http

frontend AETA
        bind *:80
        mode http
        default_backend docker

backend nodes
        mode http
        balance roundrobin
        server ww1 192.168.131.189:80 check weight 50
        server ww2 192.168.131.231:80 check weight 50

backend docker
        mode http
        balance roundrobin
        server dwww1 192.168.130.6:32776
        server dwww2 192.168.130.6:32777

listen stats
        bind *:1936
        stats enable
        stats uri /
        stats hide-version
        stats auth raphaesl:boing
        stats auth saraslu:boing
```


### Docker Swarm

On manager:
```
docker swarm init --advertise-addr 192.168.130.103
```
On workers:
```
docker swarm join --token SWMTKN-1-0wljbwbm6k4zd2yqtypryu6us24jvnfg231j3ztnpy24u9uv3s-496ykad1sirmhbjaq895yy8xq 192.168.130.103:2377
```


### Format and Mount Volume

```
# Format Volume
mkfs --type=ext4 /dev/vdb 

# Create folder
sudo mkdir /backup

# Mount volume to folder
mount /dev/vdb /backup

# To see status of volumes
df -h
```